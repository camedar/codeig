var $baseUrl = 'urlBase';

var app = angular.module('myApp', []);

/*app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "myController"});
  $scope.name = 'Camilo';
  $scope.surname = 'Medina';
}]);
 */ 
app.service('dataService', function($http) {
	this.getData = function(params, path, callbackFunc){
		$http({
			method: 'GET',
			url: $baseUrl + path,
			params: params
        	/// headers: {'Authorization': 'Token token=xxxxYYYYZzzz'}
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

    this.setData = function(params, path, callbackFunc){
        $.ajax({
            method: 'POST',
            url: $baseUrl + path,
            data: params
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

});

app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "myController"});
  $scope.from = {};
}]);
var $baseUrl = $('div#base-url').html();

var app = angular.module('myApp', []);

app.directive("testTemplate", function() {
    return {
        template : "<h2>Hecho con una directiva!</h2>"
    };
});

app.directive("testParametros", function() {
	return {
		scope: {
			testParametros: '='
		},
   template: "<h3> {{ testParametros.value }} </h3>"
	};
});

app.directive("testFuncion", function() {
	return {
    require: "ngModel",
		scope: {
			testFuncion: '='
		},
    template: "<h3> {{ testFuncion }} </h3>",
		link: function(scope, element, attrs, ctrl) {
       scope.testFuncion += " mas la directiva";
		}    
	};
});

app.directive("testFuncion2", function() {
	return {
    require: "ngModel",
		scope: {
      testValue: '='
		},
    template: "<h3> {{ testValue }} </h3>",
		link: function(scope, element, attrs, ctrl) {
       scope.testValue = "Cambio de valor"
		}    
	};
});

app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "Ejemplo directivas"});
  $scope.algunValor = "Valor Enviado en el controlador";
  $scope.algunOtroValor = "Valor inicial";

}]);
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

@Component({
  selector: 'myapp',
  template: `<div *ngFor="let o of obj">
      <h2>{{titulo}}</h2>
      <p>{{mensaje}}</p>
      <p>{{o.e}}</p>
    </div>
  `
})
class MyappComponent {
  titulo: string;
  mensaje: string;
  obj: object[];
  constructor () {
    this.titulo = 'titulo de prueba';
    this.mensaje = 'Mensaje de prueba';
    this.obj = [{e: "1"},{e:"2"},{e:"4"}];
  }
}

let myapp = new MyappComponent();

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);
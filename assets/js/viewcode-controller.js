var $baseUrl = $("#base-url").html();
var viewCodeApp = angular.module('viewCodeApp', ['ngMaterial']);

viewCodeApp.service('dataService', function($http) {
	this.getData = function(params, path, callbackFunc){
		$http({
			method: 'GET',
			url: $baseUrl + path,
			params: params
        	/// headers: {'Authorization': 'Token token=xxxxYYYYZzzz'}
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

    this.setData = function(params, path, callbackFunc){
        $.ajax({
            method: 'POST',
            url: $baseUrl + path,
            data: params
        }).then(function(response) {
            callbackFunc(response);
        }, function(reason) {
            console.log({'Failed: ': reason, 'values': params, 'url': path});
        }, function(update) {
            console.log({'Got notification: ':  update, 'values': params, 'url': path});
        });
    };

});

viewCodeApp.controller('viewCodeController', ['$scope', '$timeout', '$filter', 'dataService', function($scope, $timeout, $filter, dataService){
  console.log({"Controlador": "viewCodeController"});
  $scope.code = {
    title: "Javascript",
    type: "language-css", // -css, -php, -javascript - nginx
    content: "p { color: blue }"
};
}]);

viewCodeApp.controller('directivasController', ['$scope', '$timeout', '$filter', '$sce', 'dataService', function($scope, $timeout, $filter, $sce, dataService){
  console.log({"Controlador": "directivasController"});
  $scope.tabs = [
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/y8obm30e/"),title:"Agregar elemento"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/ba7xng6y/"),title:"Uso de directivas"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/ba7xng6y/"),title:"Uso de directivas"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/ba7xng6y/2/"),title:"Uso de directivas"},
	{content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/ba7xng6y/3/"),title:"Uso de directivas"}
  ];
}]);

viewCodeApp.controller('extrasController', ['$scope', '$timeout', '$filter', '$sce', 'dataService', function($scope, $timeout, $filter, $sce, dataService){
  console.log({"Controlador": "extrasController"});
  $scope.tabs = [
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/w2z4xem1/"),title:"ES5 - GET SET"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/3xffj5o3/"),title:"ES5 - Object keys"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/fzyea87n/"),title:"ES6 (2015) - let var const"},
  {content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/c6rLto72/"),title:"Es6 (2015) - arrow functions"},
	{content:$sce.trustAsResourceUrl("https://jsfiddle.net/brisan/5qdg55oj/"),title:"Es6 (2015) - Classes"}
  ];
}]);

viewCodeApp.controller('tsbasicController', ['$scope', '$timeout', '$filter', '$sce', 'dataService', function($scope, $timeout, $filter, $sce, dataService){
  console.log({"Controlador": "tsbasicController"});
  $scope.tabs = [
		{content:$sce.trustAsResourceUrl($baseUrl + "/curso/resultado/ts-install"),title:"Instalacion Compilador TypeScript", height:"1600"},
		{content:$sce.trustAsResourceUrl("https://embed.plnkr.co/bAmbhBXr7hSWFjiokbOK/"),title:"Base Angular 2", height:"800"}
  ];
}]);

viewCodeApp.controller('tsApp1', ['$scope', '$timeout', '$filter', '$sce', 'dataService', function($scope, $timeout, $filter, $sce, dataService){
  console.log({"Controlador": "tsApp1"});
  $scope.tabs = [
  	{content:$sce.trustAsResourceUrl($baseUrl + "/curso/resultado/ts-app1"),title:"Estructura", height:"800"},
		{content:$sce.trustAsResourceUrl("https://embed.plnkr.co/bAmbhBXr7hSWFjiokbOK/"),title:"Editor", height:"800"},
		{content:$sce.trustAsResourceUrl("https://codecraft.tv/courses/angular/quickstart/first-app/"),title:"Fuente", height:"800"}
		
  ];
}]);


viewCodeApp.controller('tsApp2', ['$scope', '$timeout', '$filter', '$sce', 'dataService', function($scope, $timeout, $filter, $sce, dataService){
  console.log({"Controlador": "tsApp2"});
  $scope.tabs = [
		{content:$sce.trustAsResourceUrl($baseUrl + "/curso/resultado/ts-app2-preliminar"),title:"Preliminares", height:"800"},
  	{content:$sce.trustAsResourceUrl($baseUrl + "/curso/resultado/ts-app2"),title:"Teoria", height:"800"},
		{content:$sce.trustAsResourceUrl("https://embed.plnkr.co/pDiGiy/"),title:"Editor", height:"800"},
		{content:$sce.trustAsResourceUrl($baseUrl + "/curso/resultado/ts-app2-test"),title:"Resultado", height:"800"}
  ];
}]);


<div class="container">
	<a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Inicio</a>
	<br>
	<br>
	<div id="base-url" class="hide">
		<?php echo base_url(); ?>
	</div>
</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp" ng-controller="tsbasicController">
	<div class="col-md-12">
		<div class="thumbnail">
			
			<md-content class="md-padding">
				<md-tabs md-selected="selectedIndex" md-dynamic-height md-border-bottom md-autoselect>
					<md-tab ng-repeat="tab in tabs" label="{{tab.title}}">
						<div class="demo-tab tabs{{$index%4}}">
							<div>
								<iframe ng-src="{{ tab.content }}" width="100%" frameborder="0" scrolling="auto" height="{{tab.height}}"></iframe>
							</div>
							
						</div>
					</md-tab>
				</md-tabs>
			</md-content>
			
		</div>
	</div>
</div>
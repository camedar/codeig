<div class="container-fluid">
	<div class="container">
		<!--a class="btn btn-default" href="<?php echo base_url("/assets/files/ambientedesarrollocodeanywhere.pdf"); ?>" role="button" target="_blank">Configuración sitio</a>
		<a class="btn btn-default" href="<?php echo base_url("/assets/files/proyecto1.pdf"); ?>" role="button" target="_blank">Primer proyecto</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/basicos"); ?>" role="button">Ejemplos basicos Javascript</a>
		<a class="btn btn-default" href="<?php echo base_url("/curso/formulario"); ?>" role="button">Formulario</a-->
			<br>
			<br>
			<div id="base-url" class="hide">
				<?php echo base_url(); ?>
			</div>
		</div>
		<div class="container">
			
			<div class="list-group">
				<b class="list-group-item active">
					<h4 class="list-group-item-heading">Introducción - goo.gl/xWqDKf - goo.gl/F2vYCn</h4>
					<p class="list-group-item-text">Basicos</p>
				</b>
				<i class="list-group-item">
					<h4 class="list-group-item-heading">Configuracion de sitio</h4>
					<a href="<?php echo base_url("/assets/files/ambientedesarrollocodeanywhere.pdf"); ?>" target="_blank" class="list-group-item">
						<p class="list-group-item-text">Montar servidor</p>
					</a>
					<a href="<?php echo base_url("/assets/files/proyecto1.pdf"); ?>" target="_blank" class="list-group-item">
						<p class="list-group-item-text">Configurar servidor</p>
					</a>
				</i>
				<i class="list-group-item">
					<h4 class="list-group-item-heading">Ejemplos basicos</h4>
					<a href="<?php echo base_url("/curso/basicos/javascript"); ?>" class="list-group-item">
						<p class="list-group-item-text">Javascript</p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/jquery"); ?>" class="list-group-item">
						<p class="list-group-item-text">Jquery</p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/angularjs"); ?>" class="list-group-item">
						<p class="list-group-item-text">AngularJS</p>
					</a>
				</i>
				<i class="list-group-item">
					<h4 class="list-group-item-heading">Funcionalidades AngularJS</h4>
					<a href="<?php echo base_url("/curso/basicos/angular-services1"); ?>" class="list-group-item">
						<p class="list-group-item-text">Servicios <small>(Services)</small></p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/angular-directives1"); ?>" class="list-group-item">
						<p class="list-group-item-text">Directivas <small>(Directives)</small></p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/angular-filters1.php"); ?>" class="list-group-item">
						<p class="list-group-item-text">Filtros <small>(Filters)</small></p>
					</a>
					<a href="https://www.w3schools.com/angular/tryit.asp?filename=try_ng_app5" target="_blank" class="list-group-item">
						<p class="list-group-item-text">Aplicación </p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/javascript-extras"); ?>" class="list-group-item">
						<p class="list-group-item-text">Extras </p>
					</a>
				</i>
				<b class="list-group-item active">
					<h4 class="list-group-item-heading">AngularJS 2/4</h4>
					<p class="list-group-item-text">Proyecto</p>
				</b>
				<i class="list-group-item">
					<a href="<?php echo base_url("/curso/basicos/typescript-basicos"); ?>" class="list-group-item">
						<p class="list-group-item-text">Basicos TypeScript</p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/angular2-app1"); ?>" class="list-group-item">
						<p class="list-group-item-text">Primer App Angular 2</p>
					</a>
					<a href="<?php echo base_url("/curso/basicos/angular2-app2"); ?>" class="list-group-item">
						<p class="list-group-item-text">Interpolación y bucles</p>
					</a>
				</i>
			</div>
		</div>
	</div>
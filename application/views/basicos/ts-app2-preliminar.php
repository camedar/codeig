<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Angular 2 Preliminares</title>
 <link href="<?php echo base_url("assets/vendor/node_modules/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet"/>
  <link href="<?php echo base_url("/assets/vendor/prism/prism.css"); ?>" rel="stylesheet">
</head>
<body>
  <h1>Angular 2 Preliminares</h1>
	
	<div class="col-md-4">
			<legend>
				<h2>
					HTML
				</h2>
			</legend>
			<pre class="language-markup line-numbers" data-line="14-18" style="height:500px;" data-src="<?php echo base_url("/curso/resultado/ts-app2-test"); ?>"></pre>
		</div>
		
	<div class="col-md-4">
			<legend>
				<h2>
					Systemjs
				</h2>
			</legend>
			<pre class="language-javascript line-numbers" data-line="" style="height:500px;" data-src="<?php echo base_url("/assets/js/angular2/systemjs.config.js"); ?>"></pre>
		</div>
	
	<div class="col-md-4">
			<legend>
				<h2>
					Script TS
				</h2>
			</legend>
			<pre class="language-typescript line-numbers" data-line="" style="height:500px;" data-src="<?php echo base_url("/assets/js/basicos/angular2/script-interpolacion.ts"); ?>"></pre>
		</div>
	
</body>
	<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Prims -->
<script src="<?php echo base_url("/assets/vendor/prism/prism.js"); ?>" rel="stylesheet" ></script>

<!-- Bootstrap -->
<script src="<?php echo base_url("/assets/vendor/node_modules/bootstrap/dist/js/bootstrap.min.js"); ?>" rel="stylesheet"></script>
</html>
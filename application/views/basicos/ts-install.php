<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Instalacion TypeScript</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body >
	<div class="container-fluid">
		<div class="container">
			<div id="base-url" class="hide">
				<?php echo base_url(); ?>
			</div>
			<h1>Instalacion TypeScript</h1>
			<pre>npm install -g typescript</pre>
			<br>
			<h3>
				Comprobar instalación
			</h3>
			<pre>tsc --version</pre>
			<br>
			<h3>
				Compilar
			</h3>
			<pre>tsc archivo.ts</pre>
			<p>
				El comando tsc cuenta con una serie de comandos que permiten modificar determinadas opciones se puede obtener con el comando <pre>tsc --help</pre>
				Pero en consola determinar muchos comandos es una tarea engorrosa, por este motivo las opciones se pueden configurar de forma predefinida en un archivo llamado tsconfig.json
			</p>
			<h3>
				Archivo tsconfig.json
			</h3>
			<pre>
{
	"compilerOptions": {
		"target": "es5",
		"module": "commonjs",
		"moduleResolution": "node",
		"sourceMap":  true
	}
}
	</pre>
		<p>
			El archivo en un json que contiene todas las opciones que deseamos incluir por defecto para transcompilar los archivos ts
		</p>
		<h3>
			Ejemplos archivos
		</h3>
		<pre>
let name:string = "Brian";

console.log(name);
		</pre>
		<pre>
class Animal {
    constructor(public name) { }
    move(meters) {
        console.log(this.name + " se movió " + meters + "m.");
    }
}

class Snake extends Animal {
    move() {
        console.log("Deslizándose...");
        super.move(5);
    }
}

class Horse extends Animal {
    move() {
        console.log("Galopando...");
        super.move(45);
    }
}

var sam = new Snake("Sammy la Pyton")
var tom: Animal = new Horse("Tommy el Palomino")

sam.move()
tom.move(34)
		</pre>
</div>
</div>
</body>
<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Boopttrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-messages.js"></script>

<!-- My JS Remplazar con la ruta propia usando el metodo de en php base_url() -->
<!--script src="<?php echo base_url("/assets/js/basicos/ts-install.js"); ?>" rel="stylesheet"></script-->
</html>
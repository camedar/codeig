<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo Angular</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body ng-app="myApp" ng-controller="myController">
	<h1>
    Angular
  </h1>
	<a class="btn btn-primary" href="#" role="button">Volver</a>
	<br/><br/>
	<div>
		Ingrese nombre:
		<input type="text" name="name" id="name" ng-model="from.name"></input>
	</div>
	<div id="output">Nombre {{from.name}}</div>
	
	<br/><br/>
	<div>
		Ingrese Edad:
		<input type="text" name="age" id="age" ng-model="from.age"></input>
	</div>
	<div id="output">Edad {{from.age}}</div>

<br/><br/>
	<div>
		Ingrese nombre:
		<input type="text" name="address" id="address" ng-model="from.address"></input>
	</div>
	<div id="output">Direccion {{from.address}}</div>
</body>
<!-- Boopttrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="<?php echo base_url("/assets/js/basicos/angular.js"); ?>" rel="stylesheet"></script>
</html>